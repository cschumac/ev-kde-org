---
title: "KDE e.V. Fundraising Working Group"
layout: page
---

### Goals

The KDE e.V. Fundraising Working Group has the following tasks:

+ Identify fundraising opportunities and coordinate the execution of fundraising campaigns.
+ Maintain and improve the technical infrastructure used in fundraising campaigns.
+ Coordinate the production and outreach of promotional material for fundraising campaigns.

### Members

The current members of the KDE e.V. Fundraising Working Group are:

+ Kenny Coyle
+ Lays Rodrigues
+ Scarlett Clark
+ Carl Schwan

### Contact 

You can contact the Fundraising Working Group under [kde-ev-campaign@kde.org](mailto:kde-ev-campaign@kde.org).
