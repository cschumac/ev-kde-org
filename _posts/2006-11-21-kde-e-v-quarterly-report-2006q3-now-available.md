---
title: 'KDE e.V. Quarterly Report 2006Q3 Now Available'
date: 2006-11-21 00:00:00 
layout: post
---

The <a href="http://ev.kde.org/reports/ev-quarterly-2006Q3.pdf">KDE e.V.
Quarterly Report</a> is now available for July to September 2006.  Topics covered
include the outcomes from the <a href="/reports/2006/">2006
membership meeting</a>, the status of the Technical Working Group's
<a href="/rules/twg_charter/">improved charter</a>, the
new press channel from the Marketing Working Group and for the first time a report
from the Sysadmin Team.  All long term KDE contributors are welcome to
<a href="/getinvolved/members/">join KDE e.V</a>.
