---
title: 'KDE e.V. Quarterly Report 2011 Q3'
date: 2011-11-23 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2011_Q3.pdf">third quarterly report of 2011</a>.

This report covers July through September 2011, including statements from the board, reports from sprints, financial information and an interview with Lydia Pintscher about why KDE rocks at mentoring. A special report about the Desktop Summit and the annual General Assembly is included as well.
      
