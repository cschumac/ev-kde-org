---
title: 'KDE e.V. Report for 2017'
date: 2018-06-27 00:00:00 
layout: post
---

<p>KDE e.V., the non-profit organisation behind the KDE community, is happy to present the <a href="https://ev.kde.org/reports/ev-2017/" target="_blank">KDE e.V. Community Report for 2017</a>.</p>
	<p>The report gives a comprehensive overview of all that has happened during 2017. It covers the progress we have made with KDE's Plasma desktop environment; Plasma Mobile (KDE's graphical environment for mobile devices); and applications the community creates to stimulate your productivity, creativity, education, and fun. It also looks at KDE's activities during 2017, giving details on the results from community sprints, conferences, and external events the KDE community has participated in worldwide. You can also find out about the inner workings of KDE e.V., the foundation that legally represents the community. Check KDE's financial status and read up about the KDE e.V. board members, the different working groups, the Advisory Board, and how they all work together to keep KDE moving forward.</p>
      