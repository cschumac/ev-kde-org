---
title: 'KDE e.V. Quarterly Report 2014 Q3'
date: 2015-02-12 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2014_Q3.pdf">report for the third quarter of 2014</a>.

This report covers July through September 2014, including statements from the board, reports from sprints and a feature article about the Visual Design Group.
      
