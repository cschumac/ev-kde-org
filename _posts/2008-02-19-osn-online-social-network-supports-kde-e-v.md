---
title: 'OSN Online Social Network supports KDE e.V.'
date: 2008-02-19 00:00:00 
layout: post
---

<a href="http://verwandt.de">OSN Online Social Network GmbH</a> has joined
the ranks of KDE e.V.'s Supporting Members. The KDE e.V. uses these financial contributions
for example for supporting developer meetings which are a vital part of KDE's
community-building and -binding effort. Warm thanks go out to OSN as well as our other
Supporting Members and Patrons for their support of KDE.
      
