---
title: 'KDE e.V. Quarterly Report 2014 Q1 and Q2'
date: 2014-10-11 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2014_Q1Q2.pdf">report for the first and second quarter of 2014</a>.

This report covers January through June 2014, including statements from the board, reports from sprints and a feature article about Plasma 5.
      
